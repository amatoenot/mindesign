Как собрать:

1. Установка зависимостей
```composer install```
2. Прописать параметры mysql в 'parameters.yml'
3. Создать бд, обновить схему и накатить фикстуры
```bin/console doctrine:database:create```
```bin/console doctrine:schema:update```
```bin/console doctrine:fixtures:load```
4. Запустить сервер
```bin/console server:start```

Фикстуры создают пользователя admin:admin.

Пользователи:
```/admin/user```
Проекты:
```/project```
Роли:
```/admin/accessrole```
