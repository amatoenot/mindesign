<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AccessRole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Accessrole controller.
 *
 * @Route("admin/accessrole")
 */
class AccessRoleController extends Controller
{
    /**
     * Lists all accessRole entities.
     *
     * @Route("/", name="accessrole_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $accessRoles = $em->getRepository('AppBundle:AccessRole')->findAll();

        return $this->render('accessrole/index.html.twig', array(
            'accessRoles' => $accessRoles,
        ));
    }

    /**
     * Creates a new accessRole entity.
     *
     * @Route("/new", name="accessrole_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $accessRole = new Accessrole();
        $form = $this->createForm('AppBundle\Form\AccessRoleType', $accessRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accessRole);
            $em->flush();

            return $this->redirectToRoute('accessrole_show', array('id' => $accessRole->getId()));
        }

        return $this->render('accessrole/new.html.twig', array(
            'accessRole' => $accessRole,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a accessRole entity.
     *
     * @Route("/{id}", name="accessrole_show")
     * @Method("GET")
     */
    public function showAction(AccessRole $accessRole)
    {
        $deleteForm = $this->createDeleteForm($accessRole);

        return $this->render('accessrole/show.html.twig', array(
            'accessRole' => $accessRole,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing accessRole entity.
     *
     * @Route("/{id}/edit", name="accessrole_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AccessRole $accessRole)
    {
        $deleteForm = $this->createDeleteForm($accessRole);
        $editForm = $this->createForm('AppBundle\Form\AccessRoleType', $accessRole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('accessrole_edit', array('id' => $accessRole->getId()));
        }

        return $this->render('accessrole/edit.html.twig', array(
            'accessRole' => $accessRole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a accessRole entity.
     *
     * @Route("/{id}/delete", name="accessrole_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AccessRole $accessRole)
    {
        $form = $this->createDeleteForm($accessRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($accessRole);
            $em->flush();
        }

        return $this->redirectToRoute('accessrole_index');
    }

    /**
     * Creates a form to delete a accessRole entity.
     *
     * @param AccessRole $accessRole The accessRole entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AccessRole $accessRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accessrole_delete', array('id' => $accessRole->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
