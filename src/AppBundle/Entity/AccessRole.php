<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AccessRole
 *
 * @ORM\Table(name="access_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccessRoleRepository")
 */
class AccessRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="isFullAccess", type="boolean")
     */
    private $isFullAccess = false;

    /**
     * @var string
     *
     * @ORM\Column(name="accessLevel", type="string", length=255)
     */
    private $accessLevel;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project")
     *
     * @var Collection
     */
    private $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isFullAccess.
     *
     * @param bool $isFullAccess
     *
     * @return AccessRole
     */
    public function setIsFullAccess($isFullAccess)
    {
        $this->isFullAccess = $isFullAccess;

        return $this;
    }

    /**
     * Get isFullAccess.
     *
     * @return bool
     */
    public function getIsFullAccess()
    {
        return $this->isFullAccess;
    }

    /**
     * Set accessLevel.
     *
     * @param string $accessLevel
     *
     * @return AccessRole
     */
    public function setAccessLevel($accessLevel)
    {
        $this->accessLevel = $accessLevel;

        return $this;
    }

    /**
     * Get accessLevel.
     *
     * @return string
     */
    public function getAccessLevel()
    {
        return $this->accessLevel;
    }

    /**
     * @return Collection
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param Collection $projects
     */
    public function setProjects(Collection $projects): void
    {
        $this->projects = $projects;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


}
