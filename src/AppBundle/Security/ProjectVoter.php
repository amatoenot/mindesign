<?php

declare(strict_types=1);


namespace AppBundle\Security;


use AppBundle\Entity\AccessRole;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProjectVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Project) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        $accessRole = $user->getAccessRole();

        if (null === $accessRole) {
            return false;
        }

        // you know $subject is a Post object, thanks to supports
        /** @var Project $project */
        $project = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($project, $accessRole);
            case self::EDIT:
                return $this->canEdit($project, $accessRole);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Project $project, AccessRole $accessRole)
    {
        // if they can edit, they can view
        if ($this->canEdit($project, $accessRole)) {
            return true;
        }

        if (self::VIEW !== $accessRole->getAccessLevel()) {
            return false;
        }

        if ($accessRole->getIsFullAccess()) {
            return true;
        }

        return $accessRole->getProjects()->contains($project);
    }

    private function canEdit(Project $project, AccessRole $accessRole)
    {
        if (self::EDIT !== $accessRole->getAccessLevel()) {
            return false;
        }

        if ($accessRole->getIsFullAccess()) {
            return true;
        }

        return $accessRole->getProjects()->contains($project);
    }
}